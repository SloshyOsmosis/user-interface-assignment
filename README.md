# User-Interface-Assignment

In this readme I will note down anything useful that the reader needs to know!

1. The page editor cant be accessed through the other pages and must be loaded on its own, there is no links to the page editor as this is only a user interface with minimal functionality! The page editor can be found via "index.html"
    1a. "profile-view.html" is considered the main page.

2. Images generated in the main page are random. I have used the following links to access the api's:

https://www.vectorstock.com/royalty-free-vector/silhouette-of-city-with-red-color-vector-8127791 - Cover image used in the edit profile page

https://random.imagecdn.app - Random Cover Image

https://xsgames.co/randomusers - Random male and female Profile Pictures

https://dribbble.com/shots/14965855-Simple-Character-of-Muslim-Girl - Image used on the search page. All credit goes to Amelia Hideyat

3. The 'Sign up' and 'Log in' Links on the navigation bar will direct you to the sign-up and log-in forms. To redirect back to the main page, click on the Leeds Trinity icon. The search tab on the header will also direct you to the search page.

4. To load the webpage, run the app.py flask program.

5. These are the required libraries needed to run the program:
    flask
    mysql-connector-python
    flask_wtf
    wtforms
    

6. To load the database, you need to right click on the 'tstack_database.sql' file and open with MySQL Workbench. The password you need is '1234554321'